use itertools::Itertools;

fn first_unique(input: &str, n: usize) -> usize {
    input
        .chars()
        .collect_vec()
        .windows(n)
        .map(|x| x.iter().all_unique())
        .position(|x| x.to_owned())
        .unwrap()
        + n
}

pub fn part1(input: &str) -> i32 {
    first_unique(input, 4) as i32
}

pub fn part2(input: &str) -> i32 {
    first_unique(input, 14) as i32
}
