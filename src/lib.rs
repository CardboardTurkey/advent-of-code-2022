pub mod day1;
pub mod day2;
pub mod day3;
pub mod day4;
pub mod day5;
pub mod day6;
// pub mod day7;
pub mod day8;
pub mod day9;

#[cfg(test)]
mod tests {
    use super::*;
    use rstest::rstest;
    use std::path::PathBuf;

    type SolutionInt = fn(&str) -> i32;
    type SolutionString = fn(&str) -> String;

    #[rstest]
    #[case::day1_part1("day1", day1::part1, 24000, 66487)]
    #[case::day1_part2("day1", day1::part2, 45000, 197301)]
    #[case::day2_part1("day2", day2::part1, 15, 10718)]
    #[case::day2_part2("day2", day2::part2, 12, 14652)]
    #[case::day3_part1("day3", day3::part1, 157, 7831)]
    #[case::day3_part2("day3", day3::part2, 70, 2683)]
    #[case::day4_part1("day4", day4::part1, 2, 456)]
    #[case::day4_part1("day4", day4::part2, 4, 808)]
    #[case::day6_part1("day6", day6::part1, 11, 1833)]
    #[case::day6_part2("day6", day6::part2, 26, 3425)]
    // #[case::day7_part1("day7", day7::part1, 95437, 1833)]
    #[case::day8_part1("day8", day8::part1, 21, 1688)]
    #[case::day8_part2("day8", day8::part2, 8, 410400)]
    #[case::day9_part1("day9", day9::part1, 13, 6175)]
    #[case::day9_part2("day9", day9::part2, 1, 0)]
    fn solutions_int(
        #[case] day: &str,
        #[case] solution: SolutionInt,
        #[case] expected_mini: i32,
        #[case] expected_long: i32,
    ) {
        let input_path: PathBuf = ["input", day, "test.txt"].iter().collect();
        let input = std::fs::read_to_string(input_path).unwrap();
        assert_eq!(solution(&input), expected_mini);
        let input_path: PathBuf = ["input", day, "problem.txt"].iter().collect();
        let input = std::fs::read_to_string(input_path).unwrap();
        assert_eq!(solution(&input), expected_long);
    }

    #[rstest]
    #[case::day5_part1("day5", day5::part1, "CMZ", "ZBDRNPMVH")]
    #[case::day5_part2("day5", day5::part2, "MCD", "WDLPFNNNB")]
    fn solutions_string(
        #[case] day: &str,
        #[case] solution: SolutionString,
        #[case] expected_mini: &str,
        #[case] expected_long: &str,
    ) {
        let input_path: PathBuf = ["input", day, "test.txt"].iter().collect();
        let input = std::fs::read_to_string(input_path).unwrap();
        assert_eq!(solution(&input), expected_mini);
        let input_path: PathBuf = ["input", day, "problem.txt"].iter().collect();
        let input = std::fs::read_to_string(input_path).unwrap();
        assert_eq!(solution(&input), expected_long);
    }
}
