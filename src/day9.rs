use std::{collections::HashSet, error};

use itertools::Itertools;
use ndarray::{arr1, Array1};
use ndarray_stats::DeviationExt;

#[derive(Debug, Clone)]
enum Instruction {
    Right(i32),
    Left(i32),
    Up(i32),
    Down(i32),
}

impl TryFrom<&str> for Instruction {
    type Error = Box<dyn error::Error>;

    fn try_from(line: &str) -> Result<Self, Self::Error> {
        let (direction, num_str): (&str, &str) =
            line.split_whitespace().collect_tuple().ok_or("Hi")?;
        let params: (char, i32) = (direction.parse()?, num_str.parse()?);
        let instruction = match params.0 {
            'R' => Instruction::Right(params.1),
            'L' => Instruction::Left(params.1),
            'U' => Instruction::Up(params.1),
            'D' => Instruction::Down(params.1),
            _ => todo!(),
        };
        Ok(instruction)
    }
}

#[derive(Debug)]
struct Program {
    instructions: Vec<Instruction>,
    rope: Vec<Array1<i32>>,
    occupied_positions: HashSet<Array1<i32>>,
}

impl Program {
    fn rope_length(&mut self, len: usize) -> Result<(), &str> {
        match len {
            len if len < 2 => Err("you do bad"),
            len => Ok(self.rope = vec![arr1(&[0, 0]); len]),
        }
    }

    fn simulate(&mut self) {
        self.instructions.clone().iter().for_each(|x| match x {
            Instruction::Right(num) => self.move_head(arr1(&[1, 0]), num),
            Instruction::Left(num) => self.move_head(arr1(&[-1, 0]), num),
            Instruction::Up(num) => self.move_head(arr1(&[0, 1]), num),
            Instruction::Down(num) => self.move_head(arr1(&[0, -1]), num),
        })
    }

    fn num_positions(&self) -> usize {
        self.occupied_positions.len()
    }

    fn move_head(&mut self, increment: Array1<i32>, num: &i32) {
        for _ in 0..*num {
            for index in 0..self.rope.len() {
                self.rope[index] = &self.rope[index] + &increment;
                if self.rope[index].l2_dist(&self.rope[index + 1]).unwrap() > 2f64.sqrt() {
                    self.rope[index + 1] = &self.rope[index] - &increment;
                }
                if dbg!(index + 2) == dbg!(self.rope.len()) {
                    self.occupied_positions.insert(self.rope[index + 1].clone());
                    break;
                }
            }
        }
    }
}

impl From<&str> for Program {
    fn from(input: &str) -> Self {
        let instructions = input
            .lines()
            .filter_map(|x| Instruction::try_from(x).ok())
            .collect_vec();
        let mut occupied_positions = HashSet::new();
        occupied_positions.insert(arr1(&[0, 0]));
        Program {
            instructions,
            rope: vec![arr1(&[0, 0]), arr1(&[0, 0])],
            occupied_positions,
        }
    }
}

pub fn part1(input: &str) -> i32 {
    let mut simulator: Program = input.into();
    simulator.simulate();
    simulator.num_positions() as i32
}

pub fn part2(input: &str) -> i32 {
    let mut simulator: Program = input.into();
    simulator.rope_length(10).ok();
    simulator.simulate();
    simulator.num_positions() as i32
}
