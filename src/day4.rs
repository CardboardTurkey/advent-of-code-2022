use std::ops::RangeInclusive;

use itertools::Itertools;

fn get_ranges(line: &str) -> (RangeInclusive<u32>, RangeInclusive<u32>) {
    line.split(',')
        .map(|x| {
            x.split('-')
                .map(|x| x.parse::<u32>().unwrap())
                .collect_tuple::<(u32, u32)>()
                .unwrap()
        })
        .map(|x| x.0..=x.1)
        .collect_tuple::<(RangeInclusive<u32>, RangeInclusive<u32>)>()
        .unwrap()
}

fn check_range1(pair: (RangeInclusive<u32>, RangeInclusive<u32>)) -> Option<()> {
    let range1 = pair.0;
    let range2 = pair.1;
    (range1.contains(range2.start()) && range1.contains(range2.end())
        || range2.contains(range1.start()) && range2.contains(range1.end()))
    .then_some(())
}

pub fn part1(input: &str) -> i32 {
    input
        .trim()
        .lines()
        .map(get_ranges)
        .filter_map(check_range1)
        .collect_vec()
        .len() as i32
}

fn check_range2(pair: (RangeInclusive<u32>, RangeInclusive<u32>)) -> Option<()> {
    let range1 = pair.0;
    let range2 = pair.1;
    (range1.contains(range2.start())
        || range1.contains(range2.end())
        || range2.contains(range1.start())
        || range2.contains(range1.end()))
    .then_some(())
}

pub fn part2(input: &str) -> i32 {
    input
        .trim()
        .lines()
        .map(get_ranges)
        .filter_map(check_range2)
        .collect_vec()
        .len() as i32
}
