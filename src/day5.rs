use itertools::Itertools;

type Stacks = Vec<Vec<char>>;
type Instructions = Vec<(usize, usize, usize)>;

fn get_insns(line: &str) -> (usize, usize, usize) {
    line.split_whitespace()
        .filter_map(|x| x.parse().ok())
        .collect_tuple()
        .unwrap()
}

fn stacks_and_instruction(input: &str) -> (Stacks, Instructions) {
    let mut stack_str = input.to_owned();
    let instructions_str = stack_str
        .split_off(stack_str.find("\n\n").unwrap())
        .trim()
        .to_owned();
    let num_stacks = stack_str
        .split_off(stack_str.rfind('\n').unwrap())
        .trim()
        .chars()
        .last()
        .unwrap()
        .to_digit(10)
        .unwrap() as usize;
    let mut stacks = vec![Vec::<char>::new(); num_stacks];
    (0..num_stacks).for_each(|x| {
        stack_str.lines().rev().for_each(|line| {
            stacks[x].push(line.chars().nth(x * 4 + 1).unwrap());
        });
        stacks[x].retain(|x| !x.is_whitespace());
    });
    let instructions = instructions_str.lines().map(get_insns).collect_vec();
    (stacks, instructions)
}

pub fn part1(input: &str) -> String {
    let (mut stacks, instructions) = stacks_and_instruction(input);
    for insn in instructions {
        for _num in 0..insn.0 {
            let crate_ = stacks[insn.1 - 1].pop().unwrap();
            stacks[insn.2 - 1].push(crate_);
        }
    }
    stacks.iter().map(|x| x.to_owned().pop().unwrap()).collect()
}

pub fn part2(input: &str) -> String {
    let (mut stacks, instructions) = stacks_and_instruction(input);
    for insn in instructions {
        let mut source = stacks[insn.1 - 1].clone();
        let mut dest = stacks[insn.2 - 1].clone();
        dest.extend(source.drain(stacks[insn.1 - 1].len() - insn.0..));
        stacks[insn.1 - 1] = source;
        stacks[insn.2 - 1] = dest;
    }
    stacks.iter().map(|x| x.to_owned().pop().unwrap()).collect()
}
