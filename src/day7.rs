/// Please do not read this code

use std::{collections::HashMap, path::PathBuf};

use itertools::Itertools;

pub fn part1(input: &str) -> i32 {
    let mut filesystem: HashMap<PathBuf, Vec<u32>> = HashMap::new();
    let runs = input
        .trim()
        .split("$ ")
        .filter_map(|x| (!x.is_empty()).then(|| x.to_owned()));
    let mut cwd = PathBuf::new();
    for run in runs {
        let (cmd, output) = run.split_once('\n').unwrap();
        if cmd.starts_with("cd ") {
            let dir = PathBuf::from(cmd.split_once(' ').unwrap().1);
            if dir.is_absolute() {
                cwd = dir;
            } else if dir.to_str().unwrap() == ".." {
                assert!(cwd.pop());
            } else {
                cwd.push(dir);
            }
        } else {
            for (size, _) in output
                .lines()
                .filter(|x| !x.starts_with("dir"))
                .map(|x| x.split_once(' ').unwrap())
                .map(|(size, name)| (size.parse::<u32>().unwrap(), PathBuf::from(name)))
            {
                if let Some(mut entry) = filesystem.insert(cwd.clone(), vec![size]) {
                    filesystem.get_mut(&cwd).unwrap().append(&mut entry)
                }
            }
        }
    }
    let dir_sizes: HashMap<PathBuf, u32> = filesystem
        .iter()
        .map(|(dir, sizes)| (dir.to_owned(), sizes.iter().sum()))
        .collect();
    let dirs = filesystem.keys().map(|x| x.to_owned()).collect_vec();
    for dir in &dirs {
        for dir1 in &dirs {
            if (dir1 != dir) && (dir1.starts_with(dir)) {
                filesystem.get_mut(dir).unwrap().push(dir_sizes.get(dir1).unwrap().to_owned());
            }
        }
    }
    let dir_sizes_inclusive: HashMap<PathBuf, u32> = filesystem
        .iter()
        .map(|(dir, sizes)| (dir.to_owned(), sizes.iter().sum()))
        .collect();
    dbg!(&dir_sizes_inclusive);
    dir_sizes_inclusive.iter().filter(|x| x.1<&100000).map(|x| x.1.to_owned()).sum::<u32>() as i32
}
