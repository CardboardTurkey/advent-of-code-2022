use itertools::Itertools;
use ndarray::{
    prelude::{s, ArrayBase, Dim},
    Array, OwnedRepr,
};

pub fn array_from_file(input: &str) -> ArrayBase<OwnedRepr<char>, Dim<[usize; 2]>> {
    let input = input.trim();
    let (x, y) = (input.lines().next().unwrap().len(), input.lines().count());
    let folded_grid: Vec<char> = input
        .chars()
        .filter(|c| !c.is_whitespace())
        .fold(vec![], |sum, i| [sum.as_slice(), &[i]].concat());
    Array::from_shape_vec((x, y), folded_grid).unwrap()
}

pub fn part1(input: &str) -> i32 {
    let array = array_from_file(input);
    let mut num_visible = 0;
    for ((x, y), elem) in array.indexed_iter() {
        for slice in [s![x, ..y], s![x, y + 1..], s![..x, y], s![x + 1.., y]] {
            if array
                .slice(slice)
                .iter()
                .filter(|x| *x >= elem)
                .collect_vec()
                .is_empty()
            {
                num_visible += 1;
                break;
            }
        }
    }
    num_visible
}

pub fn part2(input: &str) -> i32 {
    let array = array_from_file(input);
    let mut highest_score = 0;
    for ((x, y), elem) in array.indexed_iter() {
        let mut scenic_score = 1;
        for slice in [
            s![x,  ..y;-1],
            s![x, y + 1..],
            s![..x;-1, y],
            s![x + 1.., y],
        ] {
            let mut view = 0;
            for elem2 in array.slice(slice) {
                view += 1;
                if elem2 >= elem {
                    break;
                }
            }
            scenic_score *= view;
        }
        if scenic_score > highest_score {
            highest_score = scenic_score;
        }
    }
    highest_score
}
