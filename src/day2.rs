use itertools::Itertools;

/// A: Rock, B: Paper, C: Scissors
/// X: Rock (1), Y : Paper (2), Z: Scissors (3)
fn calculate_score1(strategy: Vec<char>) -> u32 {
    match strategy.as_slice() {
        ['A', 'X'] => 4,
        ['A', 'Y'] => 8,
        ['A', 'Z'] => 3,

        ['B', 'X'] => 1,
        ['B', 'Y'] => 5,
        ['B', 'Z'] => 9,

        ['C', 'X'] => 7,
        ['C', 'Y'] => 2,
        ['C', 'Z'] => 6,

        _ => panic!("Naughty input"),
    }
}

/// A: Rock, B: Paper, C: Scissors
/// X: Loose, Y: Draw, Z: Win
fn calculate_score2(strategy: Vec<char>) -> u32 {
    match strategy.as_slice() {
        ['A', 'X'] => 3,
        ['A', 'Y'] => 4,
        ['A', 'Z'] => 8,

        ['B', 'X'] => 1,
        ['B', 'Y'] => 5,
        ['B', 'Z'] => 9,

        ['C', 'X'] => 2,
        ['C', 'Y'] => 6,
        ['C', 'Z'] => 7,

        _ => panic!("Naughty input"),
    }
}

pub fn part1(input: &str) -> i32 {
    input
        .trim()
        .replace(' ', "")
        .lines()
        .map(|x| x.chars().collect_vec())
        .map(calculate_score1)
        .sum::<u32>() as i32
}

pub fn part2(input: &str) -> i32 {
    input
        .trim()
        .replace(' ', "")
        .lines()
        .map(|x| x.chars().collect_vec())
        .map(calculate_score2)
        .sum::<u32>() as i32
}
