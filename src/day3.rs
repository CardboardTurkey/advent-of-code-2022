use std::collections::HashSet;

use itertools::Itertools;

fn priority(char: char) -> u32 {
    match char.to_owned() as u32 {
        num if num >= 97 => num - 96,
        num => num - 38,
    }
}

fn sack_common(line: &str) -> char {
    let sack = line.chars().collect_vec();
    let half = sack.len() / 2;
    let compartment1: HashSet<char> = sack[..half].iter().cloned().collect();
    let compartment2: HashSet<char> = sack[half..].iter().cloned().collect();
    compartment1
        .intersection(&compartment2)
        .next()
        .unwrap()
        .to_owned()
}

pub fn part1(input: &str) -> i32 {
    input
        .trim()
        .lines()
        .map(sack_common)
        .map(priority)
        .sum::<u32>() as i32
}

fn group_common((sack1, sack2, sack3): (HashSet<char>, HashSet<char>, HashSet<char>)) -> char {
    let common12 = sack1
        .intersection(&sack2)
        .copied()
        .collect::<HashSet<char>>();
    common12.intersection(&sack3).next().unwrap().to_owned()
}

pub fn part2(input: &str) -> i32 {
    input
        .trim()
        .lines()
        .map(|x| x.chars().collect::<HashSet<char>>())
        .tuples::<(_, _, _)>()
        .map(group_common)
        .map(priority)
        .sum::<u32>() as i32
}
