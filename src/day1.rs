use itertools::Itertools;

pub fn part1(input: &str) -> i32 {
    input
        .trim()
        .split("\n\n")
        .map(|x| x.lines().map(|x| x.parse::<u32>().unwrap()).sum::<u32>())
        .max()
        .unwrap() as i32
}

pub fn part2(input: &str) -> i32 {
    input
        .trim()
        .split("\n\n")
        .map(|x| x.lines().map(|x| x.parse::<u32>().unwrap()).sum::<u32>())
        .sorted()
        .rev()
        .take(3)
        .sum::<u32>() as i32
}
