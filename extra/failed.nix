let
  oxalica_overlay = import (builtins.fetchTarball
    "https://github.com/oxalica/rust-overlay/archive/master.tar.gz");
  nixpkgs = import <nixpkgs> { overlays = [ oxalica_overlay ]; };
  inherit (nixpkgs) lib fetchFromGitHub rustPlatform;
  cargo-aoc = rustPlatform.buildRustPackage rec {
    pname = "cargo-aoc";
    name = "cargo-aoc";

    src = fetchTarball "https://github.com/gobanos/cargo-aoc/archive/refs/heads/master.tar.gz";

    nativeBuildInputs = [  nixpkgs.pkg-config nixpkgs.openssl ];
    buildInputs = [  nixpkgs.pkg-config nixpkgs.openssl ];
    propagatedBuildInputs = [  nixpkgs.pkg-config nixpkgs.openssl ];

    cargoSha256 = "sha256-arz8vgOTwOp1kJnz7Hqy7qsUUiaZsFkMs5eVwAJnnTY=";

  };
in
with nixpkgs;

stdenv.mkDerivation {
  name = "rust-env";
  buildInputs = [
    (rust-bin.stable.latest.default.override { extensions = [ "rust-src" ]; })
    cargo-aoc

    # Possibly needed for some crates like reqwest
    pkg-config
    openssl
  ];

  # Set Environment Variables
  RUST_BACKTRACE = 1;

}

